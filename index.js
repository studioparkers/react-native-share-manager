import {NativeModules} from 'react-native';
const {ShareManager} = NativeModules;

class Manager
{
    /**
     * Share a message
     * @param {ShareTargets} [target=ShareTargets.ALL] The platform to share to
     * @param {String} [message=""] The message to share
     * @param {String} [subject=""] The subject/title of the message
     * @param {Array<String>} [recipients=null] A list of recipients (email, phonenumbers, etc.)
     */
    static shareMessage(target = ShareTargets.ALL, message = "", subject = "", recipients = null)
    {
        return new Promise((resolve, reject)=>
        {
            const onSucces = (args)=>
            {
                console.log(`SUCCESS(${args})`);
                
                return resolve();
            };

            const onError = (args)=>
            {
                console.log(`ERROR(${args})`);
                
                return reject();
            };

            ShareManager.share(target, message, subject, onSucces, onError);
        });
    }
};

const ShareError = Object.freeze(
{
    /**
     * Unkown reason
     */
    GENRIC_ERROR: 0b00000000,
    
    /**
     * Error when the application is not available.false This usually means the application is not installed.
     */
    APPLICATION_NOT_AVAILABLE: 0b00000001,

    /**
     * Error when the application's url failed to open
     */
    UNABLE_TO_OPEN_URL_SCHEME: 0b00000010,

    SHARING_METHOD_NOT_AVAILABLE_ON_PLATFORM: 0b00000011
});

const ShareTargets = Object.freeze(
{
    ALL: "ALL",
    SMS: "SMS",
    EMAIL: "EMAIL",
    TWITTER: "TWITTER",
    FACEBOOK: "FACEBOOK",
    WHATSAPP: "WHATSAPP",
    INSTAGRAM: "INSTAGRAM"
});


export {Manager as ShareManager, ShareError, ShareTargets};
