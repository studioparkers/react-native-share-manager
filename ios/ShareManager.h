#import <UIKit/UIKit.h>
#import <React/RCTBridgeModule.h>
#import <Foundation/Foundation.h>

@interface ShareManager : NSObject <RCTBridgeModule>
{
    
}

@property(nonatomic, assign) UIViewController* viewController;

+(id)singleton;
-(void)shareMessage:(NSString*)target message:(NSString*)message subject: (NSString*)subject recipients:(NSArray*)recipients success:(RCTResponseSenderBlock)onSuccess error:(RCTResponseSenderBlock) onError;
-(void)callShareMessage:(NSString*)target message:(NSString*)message subject:(NSString*)subject recipients:(NSArray*)recipients success:(RCTResponseSenderBlock)onSuccess error:(RCTResponseSenderBlock) onError;
@end
