#import "ShareManager.h"

#import "SMSManager.h"
#import "WhatsappManager.h"

@implementation ShareManager
RCT_EXPORT_MODULE()

@synthesize viewController;

+(id)singleton
{
    static ShareManager* manager = nil;
    static dispatch_once_t once;
    dispatch_once(&once, ^{manager = [[self alloc] init];});
    return manager;
}

-(id)init
{
    if(!(self = [super init]))
        return nil;

    return self;
}

-(void)callShareMessage:(NSString*)target message:(NSString*)message subject:(NSString*)subject recipients:(NSArray*)recipients success:(RCTResponseSenderBlock)onSuccess error:(RCTResponseSenderBlock) onError
{
    ShareManager* manager = [ShareManager singleton];
    if([target isEqualToString: @"WHATSAPP"])
        manager = [WhatsappManager singleton];
    else if([target isEqualToString: @"SMS"])
        manager = [SMSManager singleton];
//    else if(strcmp(target, "EMAIL") == 0)
//        manager = [EmailManager singleton];
//    else if(strcmp(target, "FACEBOOK") == 0)
//        manager = [FacebookManager singleton];
//    else if(strcmp(target, "TWITTER") == 0)
//        manager = [TwitterManager singleton];
//

    [[NSOperationQueue mainQueue] addOperationWithBlock:^
    {
        self.viewController = [UIApplication sharedApplication].delegate.window.rootViewController;
        [manager shareMessage:target message:message subject:subject recipients:recipients success:onSuccess error:onError];
    }];
}

-(void)shareMessage:(NSString*)target message:(NSString*)message subject:(NSString*)subject recipients:(NSArray*)recipients success:(RCTResponseSenderBlock)onSuccess error:(RCTResponseSenderBlock) onError
{
    UIActivityViewController* activity = [[UIActivityViewController alloc] initWithActivityItems: @[message] applicationActivities: nil];
    [activity setTitle: subject];
    activity.excludedActivityTypes = @[UIActivityTypePrint, UIActivityTypeAssignToContact, UIActivityTypeSaveToCameraRoll];
    [activity setCompletionWithItemsHandler:^(UIActivityType activityType, BOOL completed, NSArray* returnedItems, NSError* activityError)
    {
        if(activityError != nil)
            return onError(@[@0]);
        
        if(activityType != NULL)
            return;
        
        onSuccess(@[]);
    }];
    
    [[UIApplication sharedApplication].delegate.window.rootViewController presentViewController:activity animated:YES completion:nil];
}

RCT_EXPORT_METHOD(share:(NSString*)target message:(NSString*)message subject:(NSString*)subject recipients:(NSArray*)recipients success:(RCTResponseSenderBlock)onSuccess error:(RCTResponseSenderBlock) onError)
{
    [self callShareMessage:target message:message subject:subject recipients:recipients success:onSuccess error:onError];
}
@end
