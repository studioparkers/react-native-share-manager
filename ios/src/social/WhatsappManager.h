//
//  WhatsappManager.h
//  ShareManager
//
//  Created by Dani van der Werf on 26/11/2019.
//  Copyright © 2019 Facebook. All rights reserved.
//

#ifndef WhatsappManager_h
#define WhatsappManager_h

#import "ShareManager.h"

@interface WhatsappManager : ShareManager<UIDocumentInteractionControllerDelegate>
{
    
}

+(id)singleton;
-(void)shareMessage:(NSString*)target message:(NSString*)message subject: (NSString*)subject recipients:(NSArray*)recipients success:(RCTResponseSenderBlock)onSuccess error:(RCTResponseSenderBlock) onError;
@end

#endif /* WhatsappManager_h */
