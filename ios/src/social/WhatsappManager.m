//
//  WhatsappManager.m
//  ShareManager
//
//  Created by Dani van der Werf on 26/11/2019.
//  Copyright © 2019 Facebook. All rights reserved.
//

#import "WhatsappManager.h"

@implementation WhatsappManager
+(id)singleton
{
    static WhatsappManager* manager = nil;
    static dispatch_once_t once;
    dispatch_once(&once, ^{manager = [[self alloc] init];});
    return manager;
}

-(void)shareMessage:(NSString*)target message:(NSString*)message subject:(NSString*)subject recipients:(NSArray*)recipients success:(RCTResponseSenderBlock)onSuccess error:(RCTResponseSenderBlock) onError
{
    UIApplication* application = [UIApplication sharedApplication];
    if(recipients == NULL || recipients.count < 1)
        recipients = @[@""];
    
    NSString* scheme = [NSString stringWithFormat: @"whatsapp://send?text=%@&phone=%@", message, recipients[0]];
    scheme = [scheme stringByAddingPercentEncodingWithAllowedCharacters:NSCharacterSet.URLQueryAllowedCharacterSet];
    NSURL* url = [NSURL URLWithString:scheme];
    
    if(@available(iOS 10.0, *))
    {
        if (![application respondsToSelector:@selector(openURL:options:completionHandler:)])
            return onError(@[@2]);

        [application openURL:url options:@{} completionHandler: ^(BOOL success)
        {
            if(success)
                onSuccess(@[]);
            else
                onError(@[@2]);
        }];
    }
    else
    {
        if(![application canOpenURL:url])
            return onError(@[@2]);

        [application openURL:url];
    }
}

//-(void)shareImage:(NSString*)target message:(NSString*)message subject:(NSString*)subject recipients:(NSArray*)recipients
//{
//    if ([[UIApplication sharedApplication] canOpenURL: [NSURL URLWithString:@"whatsapp://app"]]){
//
//        UIImage     * iconImage = [UIImage imageNamed:@"YOUR IMAGE"];
//        NSString    * savePath  = [NSHomeDirectory() stringByAppendingPathComponent:@"Documents/whatsAppTmp.wai"];
//
//        [UIImageJPEGRepresentation(iconImage, 1.0) writeToFile:savePath atomically:YES];
//
//        _documentInteractionController = [UIDocumentInteractionController interactionControllerWithURL:[NSURL fileURLWithPath:savePath]];
//        _documentInteractionController.UTI = @"net.whatsapp.image";
//        _documentInteractionController.delegate = self;
//
//        [_documentInteractionController presentOpenInMenuFromRect:CGRectMake(0, 0, 0, 0) inView:self.view animated: YES];
//
//
//    } else {
//        UIAlertView * alert = [[UIAlertView alloc] initWithTitle:@"WhatsApp not installed." message:@"Your device has no WhatsApp installed." delegate:self cancelButtonTitle:@"OK" otherButtonTitles:nil];
//        [alert show];
//    }
//}
@end
