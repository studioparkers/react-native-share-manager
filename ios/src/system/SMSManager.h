//
//  SMSManager.h
//  ShareManager
//
//  Created by Dani van der Werf on 26/11/2019.
//  Copyright © 2019 Facebook. All rights reserved.
//

#ifndef SMSManager_h
#define SMSManager_h

#import "ShareManager.h"
#import <MessageUI/MessageUI.h>

@interface SMSManager : ShareManager<MFMessageComposeViewControllerDelegate>
{
    
}

+(id)singleton;
-(void)shareMessage:(NSString*)target message:(NSString*)message subject: (NSString*)subject recipients:(NSArray*)recipients success:(RCTResponseSenderBlock)onSuccess error:(RCTResponseSenderBlock) onError;
@end

#endif /* SMSManager_h */
