//
//  SMSManager.m
//  ShareManager
//
//  Created by Dani van der Werf on 26/11/2019.
//  Copyright © 2019 Facebook. All rights reserved.
//

#import "SMSManager.h"

@implementation SMSManager

RCTResponseSenderBlock OnError;
RCTResponseSenderBlock OnSuccess;

+(id)singleton
{
    static SMSManager* manager = nil;
    static dispatch_once_t once;
    dispatch_once(&once, ^{manager = [[self alloc] init];});
    return manager;
}

-(void)shareMessage:(NSString*)target message:(NSString*)message subject:(NSString*)subject recipients:(NSArray*)recipients success:(RCTResponseSenderBlock)onSuccess error:(RCTResponseSenderBlock) onError
{
    NSLog(@"Send message");
    if(![MFMessageComposeViewController canSendText])
        return onError(@[@1]);
    
    if(recipients == NULL)
        recipients = @[];
    
    OnError = onError;
    OnSuccess = onSuccess;
    MFMessageComposeViewController* messageView = [[MFMessageComposeViewController alloc] init];
    messageView.messageComposeDelegate = self;
    [messageView setRecipients:recipients];
    [messageView setBody:message];
    [[UIApplication sharedApplication].delegate.window.rootViewController presentViewController:messageView animated:YES completion:nil];
}

-(void)messageComposeViewController:(MFMessageComposeViewController*)controller didFinishWithResult:(MessageComposeResult)result
{
    [controller dismissViewControllerAnimated:YES completion:nil];

    if(result == MessageComposeResultCancelled)
        OnSuccess(@[]);
    else if (result == MessageComposeResultSent)
        OnSuccess(@[]);
    else
        OnError(@[@0]);
}
@end
