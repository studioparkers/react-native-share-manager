require "json"

package = JSON.parse(File.read(File.join(__dir__, "package.json")))

Pod::Spec.new do |s|
  s.name         = "react-native-share-manager"
  s.version      = package["version"]
  s.summary      = package["description"]
  s.description  = <<-DESC
                  react-native-share-manager
                   DESC
  s.homepage     = "https://github.com/github_account/react-native-share-manager"
  s.license      = "MIT"
  # s.license    = { :type => "MIT", :file => "FILE_LICENSE" }
  s.authors      = { "Dani van der Werf" => "dani@studioparkers.nl" }
  s.platforms    = { :ios => "9.0" }
  s.source       = { :git => "https://danivdwerf@bitbucket.org/parkersapps/sharemanager_ios.git", :tag => "#{s.version}" }

  s.source_files = "ios/**/*.{h,m,swift}"
  s.requires_arc = true

  s.dependency "React"
  # ...
  # s.dependency "..."
end

